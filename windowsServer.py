import socket
import threading
import os
import sys
import subprocess
import time
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
hostname=socket.gethostname()

if len(sys.argv) == 1:
    print "usage: python windowsServer.py HOSTADDRESS"
    sys.exit()
#164.67.84.139
host=sys.argv[1]#'169.232.112.44'#my name

port = 8002
s.bind((host,port))

tempfile = "temppythonfile.py"
while(True):
    "ready to connect!"
    s.listen(5)
    c, addr = s.accept()


    #print "server. simply type your message and enter!"
    print 'got connection from ',addr

    def receive_msg():
        while True:
            string = c.recv(1024).decode()
            print "received: "+string
            if string == "FINISH" or string == "EXIT":
                return
            elif string=="START": # start marker for the start of the program.
                print "received start"
                temp = open(tempfile,"w")
            elif string == "END": # end marker for the end of the program.
                print "received END."
                outputMessages = subprocess.check_output("python "+tempfile)
                send_msg(outputMessages)
                break
            else:
                with open(tempfile,'a') as f:
                    f.write(string)


    def send_msg(outputfile):
        if outputfile is str:
            c.send(outputfile)
        else:
            for output in outputfile:
                c.send(output)
        time.sleep(1)
        c.send("FINISH")
        c.close()




    receive_msg()
#try:
#    threading.Thread(target = send_msg, args = []).start()
#    threading.Thread(target = receive_msg, args = []).start()
#except Exception,errtxt:
#    print errtxt
